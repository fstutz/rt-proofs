Require Import rt.util.all.
Require Import rt.model.time rt.model.task rt.model.job rt.model.arrival_sequence
               rt.model.priority.
Require Import rt.model.uni.schedule rt.model.uni.workload.
From mathcomp Require Import ssreflect ssrbool eqtype ssrnat seq fintype bigop.


Module Swap.

  Import UniprocessorSchedule.

  (* In this section we define the function which swaps two jobs 
     in a schedule. *)
  Section Defs.
    
    Context {Job: eqType}.

    (* Consider any arrival sequence ... *)
    Context {arr_seq: arrival_sequence Job}.
    (* ... and a schedule of these jobs. *)
    Variable sched: schedule arr_seq.

    (* Let t1 and t2 any points in time. *)
    Variable t1 t2: time.
    (* For simplicity, define some local variables. *)
    Let job_at_t1 := sched t1.
    Let job_at_t2 := sched t2.

    (* The function swap returns a schedule which is the same as 
       the original one but the job scheduled at t1 is the job at t2 
       in the original one and analog for t2 with the job at t1. *)
    Definition swap :=
      fun t =>
        if t == t2 then
          job_at_t1
        else if t == t1 then
          job_at_t2
        else sched t.

  End Defs.

  (* In this section we prove lemmas about the swap function. *)
  Section Lemmas.
    Context {Job: eqType}.

    (* Consider any arrival sequence ... *)
    Context {arr_seq: arrival_sequence Job}.
    (* ... and a schedule of these jobs. *)
    Variable sched: schedule arr_seq.

    (* In this section we prove symmetry properties of swap. *)
    Section Symmetry.

      (* Let t1 and t2 any points in time. *)
      Variable t1 t2: time.

      (* For simplicity, define some local variables. *)
      Let new_sched := swap sched t1 t2.

      (* The order of the points in time does not matter. *)
      Lemma swap_commutative:
        forall t, swap sched t2 t1 t = swap sched t1 t2 t.
      Proof.
        intros t.
        rewrite /swap.
        case: ifP => [/eqP IF1 | ELSE1].
        {
          rewrite IF1.
          case: ifP => [/eqP IF2 | ELSE2]; [by rewrite IF2 | by []].
        }
        {
          by case: ifP => [/eqP IF2 | ELSE2].
        }
      Qed.

      Corollary swap_service_commutative:
        forall j t,
          service (swap sched t2 t1) j t = service (swap sched t1 t2) j t.
      Proof.
        intros j t.
        rewrite /service /service_during /service_at /scheduled_at.
        apply eq_big_nat.
        intro i; simpl; intro LT.
        by rewrite swap_commutative.
      Qed.

      Lemma swap_same_schedule_for_same_time :
        forall t, t1 = t2 -> new_sched t = sched t.
      Proof.   
        intros t EQ.
        rewrite /new_sched. subst.
        rewrite /swap.
        case: ifP => [/eqP IF1 | ELSE1]; first by subst.
        rewrite ifN_eq; [by done| by rewrite negbT].
      Qed.  

    End Symmetry.  
    (* In this section we prove some lemmas about the service of jobs
       before the both points in time. *) 
    Section Before.

      (* Let t1 and t2 any points in time. *)
      Variable t1 t2: time.
      
      (* For simplicity, define some local variables. *)
      Let new_sched := swap sched t1 t2.
      
      (*Let t' be any point in time before t1 and t2 ... *)
      Variable t': time.
      Hypothesis H_before_t1: t' <= t1.
      Hypothesis H_before_t2: t' <= t2.
   
      (* and let j be any job, ... *)
      Variable j : JobIn arr_seq.

      (* ... then the service of j in new_sched will be the same 
             as in the original schedule. *)
      Lemma schedules_invariant_elsewhere_t1_and_t2:
        forall t, t != t1 ->
             t != t2 ->
             new_sched t = sched t.
      Proof.
        intros t NOTT1 NOTT2.
        clear H_before_t1 H_before_t2 t'.
        rewrite /new_sched.
        rewrite /swap.
        case: ifP => [/eqP IF1 | ELSE1];
          first by subst t; rewrite eq_refl in NOTT2.
        case: ifP => [/eqP IF2 | ELSE2];
        subst; [by rewrite eq_refl in NOTT1 | by done].
      Qed.
        
      
      Corollary service_invariant_before_t1_and_t2:
        forall t, t <= t1 -> t <= t2 -> service new_sched j t = service sched j t.
      Proof.
        intros t LET1 LET2.
        rewrite /service /service_during /service_at /scheduled_at.
        apply eq_big_nat.
        intro i; simpl; intro LT;
        rewrite schedules_invariant_elsewhere_t1_and_t2;
        last first.
        have NEQ2 : (i == t2) = false.
        {
          apply negbTE; rewrite neq_ltn; apply /orP; left.
          by apply: (leq_trans LT). 
        }
        by rewrite {}NEQ2.
        have NEQ1: (i == t1) = false.
        {
          apply negbTE; rewrite neq_ltn; apply /orP; left.
          by apply: (leq_trans LT). 
        }
        by rewrite {}NEQ1.
        by [].
        (* started to use wlog here, but easier with same schedule *)
        (*
        have SWAP := swap_service_commutative.
        rename H_before_t1 into BEF1, H_before_t2 into BEF2.
        generalize dependent t'. clear t'.
        move: SWAP.
        rewrite /new_sched; clear new_sched.
        rename t1 into a, t2 into b.
        wlog : a b / a <= b => [EQ |LE ].
        {
          intros SWAP t' LEA LEB.
          case /orP:  (leq_total a b) => [LEAB | LEBA].
          -by apply EQ.
          {
            rewrite -SWAP.
            apply EQ; [by done | intros j0 t | by done | by done].
            by rewrite SWAP.
          }   
        }
        {
          intros SWAP t' LEA LEB.
          apply eq_big_nat.
          intro i; simpl; intro LT.
          rewrite /service_at /service_during /service_at /scheduled_at.
          rewrite /swap.
          have : (i == b) = false and (i == a) = false.
        }  
       *)
      Qed.
    End Before.
    
    Section After.
      
      (* Let t1 and t2 any points in time. *)
      Variable t1 t2: time.
      
      (* For simplicity, define some local variables. *)
      Let new_sched := swap sched t1 t2.
      
      Variable j : JobIn arr_seq.
      Variable t : time.
      Hypothesis H_after_t1: t1 < t.
      Hypothesis H_after_t2: t2 < t.
      (*Hypothesis H_not_between: ~~(t1 < t <= t2).*)
      
      Lemma service_invariant_after_t1_and_t2:
        service new_sched j t == service sched j t.
      Proof.
        have SWAP := swap_service_commutative.
        have SERV := service_invariant_before_t1_and_t2.
        rename H_after_t1 into AFTER1, H_after_t2 into AFTER2.
        generalize dependent t. clear t.
        rewrite /new_sched; clear new_sched.
        rename t1 into a, t2 into b.
        wlog : a b / a <= b => [EQ |LE ].
        {
          intros t LEA LEB.
          case /orP:  (leq_total a b) => [LEAB | LEBA];
            [by apply EQ | by rewrite -SWAP; apply EQ].
        }
        intros t LTA LTB.
        rewrite /service /service_during /service_at /scheduled_at.
        rewrite -> big_cat_nat with (n := a); [simpl|by done|by rewrite ltnW].
        apply /eqP.
        rewrite [RHS](@big_cat_nat _ _ _ a); [simpl | by done | by apply ltnW].
        f_equal.
        apply service_invariant_before_t1_and_t2 with (t1 := a) (t2 := b); try (by done).
        rewrite leq_eqVlt in LE.
        move: LE => /orP [/eqP EQ | LT].
        {
          subst.
          apply eq_big_nat.
          intros i BIT.
          rewrite swap_same_schedule_for_same_time; by done.
        } 
        rewrite [in LHS](@big_cat_nat _ _ _ a.+1) //=.
        rewrite [in RHS](@big_cat_nat _ _ _ a.+1) //=.
        rewrite [in LHS](@big_cat_nat _ _ _ b a.+1) //=; last by apply ltnW.
        rewrite [in RHS](@big_cat_nat _ _ _ b a.+1) //=; last by apply ltnW.        
        rewrite [in LHS](@big_cat_nat _ _ _ b.+1 b) //=.
        rewrite [in RHS](@big_cat_nat _ _ _ b.+1 b) //=.
        rewrite 2!addnA addnAC -2!addnA addnC addnA addnAC -addnA.
        rewrite 2!addnA [in RHS]addnAC [in RHS]addnA [in RHS]addnAC.
        rewrite -addnA. rewrite -[in RHS]addnA [in RHS]addnC.
        f_equal; last first.
        {
          do 4 (rewrite big_nat_recl // big_geq // addn0).
          rewrite /swap.
          apply /eqP.
          case: ifP => [/eqP IF1 | ELSE1];
            first by subst a; rewrite eq_refl.
            by rewrite 2!eq_refl addnC.
        }   
        {
          have INV := schedules_invariant_elsewhere_t1_and_t2.
          f_equal.
          { 
            apply eq_big_nat.
            intros i AIB.
            rewrite INV; first by done.
            -by move: AIB => /andP [LEA _]; rewrite neq_ltn; apply /orP; right.
            -by move: AIB => /andP [_ LEB]; rewrite neq_ltn; apply /orP; left.
          }
          {
            apply eq_big_nat.
            intros i BIT.
            move: BIT => /andP [BI IT].
            rewrite INV; first by done.
            -by rewrite neq_ltn; apply /orP; right; apply ltn_trans with (n := b).
            -by rewrite neq_ltn; apply /orP; right.  
          }  
        }
      Qed.
        
    End After.
    
  End Lemmas.
End Swap.
  