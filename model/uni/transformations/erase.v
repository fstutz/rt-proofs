Require Import rt.util.all.
Require Import rt.model.time rt.model.task rt.model.job rt.model.arrival_sequence
               rt.model.priority.
Require Import rt.model.uni.schedule rt.model.uni.workload.
From mathcomp Require Import ssreflect ssrbool eqtype ssrnat seq fintype bigop.


Module Erase.

  Import UniprocessorSchedule.
    
  (* In this section we define the erase transformation for uniprocessor schedules. *)
  Section Defs.

    Context {Job: eqType}.
    
    (* Consider any job arrival sequence... *)
    Context {arr_seq: arrival_sequence Job}.

    (* ... and any uniprocessor schedule of these jobs. *)
    Variable sched: schedule arr_seq.

    (* Let t_erase be the time where a job will be erased. *)
    Variable t_erase: time.
    
    (* Then, we define the erase as the schedule in which time t is replaced by an idle time. *)
    Definition erase :=
      fun t => if t == t_erase then None else sched t. 

  End Defs.

  (* In this section we prove some lemmas about the erase transformation. *)
  Section Lemmas.

    Context {Job: eqType}.
    
    (* Consider any job arrival sequence... *)
    Context {arr_seq: arrival_sequence Job}.

    (* ... and any uniprocessor schedule of these jobs. *)
    Variable sched: schedule arr_seq.

    (* Let new_sched be the schedule produced by erasing time t_erased. *)
    Variable t_erase : time.
    Let new_sched := erase sched t_erase.

    (* First, we show that the generated schedule is the same at all times other than t_erase. *)
    Lemma erase_same_schedule_at_other_times:
      forall t, t != t_erase -> new_sched t = sched t.
    Proof.
      by intros t' NEQ; rewrite /new_sched /erase ifN_eq.
    Qed.

    (* Next, we prove lemmas for the case where there's no job to be erased. *)
    Section NothingErased.
      
      (* Assume that the original schedule is idle at the erase time. *)
      Hypothesis H_nothing_to_erase : sched t_erase = None.

      (* Then, we prove that the generated schedule remains the same. *)
      Lemma erase_none_same_schedule:
        forall t, new_sched t = sched t.
      Proof.
        intro i.
        rename H_nothing_to_erase into NONE.
        rewrite /new_sched /erase.
        by case: (boolP (i == t_erase)) => [/eqP EQ |]; first by rewrite -NONE EQ.
      Qed.
      
      (* As a result, the service received by any job also doesn't change. *)
      Lemma erase_none_same_service:
        forall j t,
          service new_sched j t = service sched j t.
      Proof.
        intros j i.
        apply eq_big_nat; intros i' LE.
        by rewrite /service_at /scheduled_at erase_none_same_schedule. 
      Qed.
      
    End NothingErased.

    (* Next, we prove some lemmas about the service before the erase time. *)
    Section BeforeEraseTime.
      
      (* At any time t no later than t_erase, ... *)
      Variable t: time.
      Hypothesis H_no_later_than_t: t <= t_erase.

      (* ...the service received by any job is the same as in the original schedule. *)
      Lemma erase_same_service_before_erase_time:
        forall j, service new_sched j t = service sched j t.
      Proof.
        intro j; symmetry.
        apply eq_big_nat.
        intros i; simpl; intro LT.
        rewrite /service_at /scheduled_at /new_sched /erase.
        have NEQ : (i == t_erase) = false.
        {
          apply negbTE; rewrite neq_ltn; apply /orP; left.
          by apply: (leq_trans LT). 
        }
        by rewrite {}NEQ.
      Qed.
      
    End BeforeEraseTime.

    (* Next, we prove that the service is preserved for jobs that were not erased. *)
    Section JobWasNotErased.

      (* Let j be any job ...*)
      Variable j: JobIn arr_seq.
      
      (* ...that was not scheduled at time t_erase in the original schedule. *)
      Hypothesis H_job_not_erased : sched t_erase != Some j.

      (* Then, the service of j at time t in the new schedule is the same as in the original one. *)
      Lemma erase_same_service_after_erase_time:
        forall t,
          service new_sched j t = service sched j t.
      Proof.
        intros t.
        case: (leqP t t_erase) => [LE | GT];
          first by apply erase_same_service_before_erase_time.
        symmetry; rewrite /service /service_during /service_at /scheduled_at.
        rewrite -> big_cat_nat with (n := t_erase); [simpl | by done | by apply ltnW ].
        rewrite [RHS](@big_cat_nat _ _ _ t_erase); [simpl | by done | by apply ltnW].
        f_equal.
        {
          apply eq_big_nat.
          intros i; simpl; intro LT.
          rewrite /new_sched /erase.
          have NEQ : (i == t_erase) = false.
          {
            apply negbTE.
            rewrite neq_ltn ; apply /orP; left.
              by apply: (leq_trans LT). 
          }
            by rewrite {}NEQ.
        }
        apply eq_big_nat.
        intros i LT.
        case: (boolP (i == t_erase)) => EQ; last by rewrite /new_sched /erase ifN_eq.
        rewrite /new_sched /erase EQ.
        move: EQ => /eqP => EQ.
        subst i ; simpl; apply /eqP.
        by rewrite eqb0.
      Qed.
        
    End JobWasNotErased.

    (* In this section, we prove some lemmas about the service after the erased time. *)
    Section AfterEraseTime.

      (* Let t be any point in time after the erased time. *)
      Variable t_after: time.
      Hypothesis H_after_t: t_after > t_erase.

      (* Now we consider the case when a job is erased. *)
      Section JobWasErased.

        (* Let j be any job that was erased. *)
        Variable j: JobIn arr_seq.
        Hypothesis H_job_was_erased: sched t_erase = Some j.

        (* Then, the service received by j after time t in the new schedule decreases by one unit. *)
        Lemma erase_service_decreases_after_erase_time:
          service new_sched j t_after = service sched j t_after - 1.
        Proof.
          rename H_after_t into AFTER.
          rewrite /service /service_during /service_at /scheduled_at.
          rewrite -> big_cat_nat with (n := t_erase); [simpl | by done | by apply ltnW ].
          rewrite [in RHS](@big_cat_nat _ _ _ t_erase); [simpl | by done | by apply ltnW].
          rewrite -addnBA; last first.
          {
            rewrite (big_rem t_erase) /=; last by rewrite mem_index_iota leqnn.
            by rewrite H_job_was_erased eq_refl.
          }
          f_equal; last first.
          {
            destruct t_after; first by rewrite ltn0 in AFTER.
            rewrite big_nat_recl // big_nat_recl //.
            rewrite H_job_was_erased eq_refl [in RHS]addnC addn1 subn1.
            rewrite -[RHS]pred_Sn.
            rewrite {1}/new_sched {1}/erase eq_refl /= add0n.
            apply eq_big_nat.
            intros i; simpl; intros LE.
            rewrite /new_sched /erase ifN_eq; first by done.
            rewrite neq_ltn; apply /orP; right.
            by move: LE => /andP [LE _].
          }
          {
            apply eq_big_nat.
            intros i; simpl; intro LT.
            rewrite /new_sched /erase.
            have NEQ : (i == t_erase) = false.
            {
              apply negbTE; rewrite neq_ltn; apply /orP; left.
              by apply: (leq_trans LT). 
            }
            by rewrite {}NEQ.
          }
        Qed.
        
      End JobWasErased.

      (* Since a job that is not erase retains its service, we can condense the lemmas about service at
         later times into the following corollary. *)
      Corollary erase_service_after_erased_time:
        forall j,
          service new_sched j t_after = service sched j t_after - (sched t_erase == Some j).
      Proof.
        intros j.
        case: (boolP (sched t_erase == Some j)) => [/eqP EQ | NEQ];
          first by apply erase_service_decreases_after_erase_time.
        by rewrite subn0; apply erase_same_service_after_erase_time.
      Qed.
      
    End AfterEraseTime.
    
  End Lemmas.
  
End Erase.