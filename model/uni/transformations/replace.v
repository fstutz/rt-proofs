Require Import rt.util.all.
Require Import rt.model.time rt.model.task rt.model.job rt.model.arrival_sequence
               rt.model.priority.
Require Import rt.model.uni.schedule rt.model.uni.workload.
From mathcomp Require Import ssreflect ssrbool eqtype ssrnat seq fintype bigop.


Module Replace.

  Import UniprocessorSchedule.
  
  Section Replace_At.
    
    Context {Job: eqType}.

    (* Consider any uniprocessor schedule. *)
    Context {arr_seq: arrival_sequence Job}.
    Variable sched: schedule arr_seq.

    Variable t : time.
    Variable j_new : JobIn arr_seq.
    
    Definition replace_at : schedule arr_seq :=
      fun t' => if  t' == t then Some j_new else sched t'.

    Lemma replace_at_invariant_on_different_times (t' : time):
      t' != t -> sched t' = replace_at t'.
    Proof.
      intro.
      by rewrite /replace_at ifN_eq.
    Qed.

    Hint Resolve replace_at_invariant_on_different_times.

    Lemma replace_at_changes_job_at_time_t (t' : time):
      t' == t -> replace_at t' = Some j_new.
    Proof.
      intros NEQ.
      by rewrite /replace_at NEQ.
    Qed.

    Lemma replace_at_no_change_if_jobs_are_equal (t' : time):
      sched t = Some j_new -> sched t' = replace_at t'.
    Proof.
      intro EQ.
      rewrite /replace_at.
      desf; last by [].
      by rewrite Heq.
    Qed.

    Hint Resolve replace_at_no_change_if_jobs_are_equal.

    Lemma replace_at_changes_schedule_if_jobs_different (t' : time):
      Some j_new != sched t -> replace_at t != sched t.
    Proof.
      intro.
      rewrite /replace_at.
      by desf.
    Qed.

    Lemma replace_at_service_at_invariant_other (t' : time) (job : JobIn arr_seq):
      t' != t -> service_at sched job t' = service_at replace_at job t'.
    Proof.
      intro.
      rewrite /replace_at /service_at /scheduled_at.
      by rewrite ifN_eq.
    Qed.

    Lemma replace_at_service_at_invariant_before (t' : time) (job : JobIn arr_seq):
      t' < t -> service_at sched job t' = service_at replace_at job t'.
    Proof.
      intro LT.
      apply replace_at_service_at_invariant_other.
      apply ltn_eqF in LT.
      by rewrite LT.
    Qed.

    (* Lemma replace_at_service_at_invariant_other_jobs (t' : time) (job : JobIn arr_seq) :
      (sched t != Some j_new) ->
      service_at sched job t' = service_at replace_at job t'.
    Proof.
      intros NOTSCHEDT.
      rewrite /service_at /scheduled_at /replace_at.
      desf; last by [].
      rewrite -Heq in NOTSCHEDT.
      apply negbTE in NOTSCHEDT.
      by rewrite NOTMAYBEJOB NOTSCHEDT.
    Qed.

    Lemma replace_at_service_invariant_before_t (t' : time) (job : JobIn arr_seq) :
      t' <= t -> service replace_at job t' = service sched job t'.
    Proof.
      intro.
      induction t' ; first by rewrite /service /service_during !big_geq.
      rewrite /service /service_during.
      rewrite big_nat_recr //= big_nat_recr //=.
      f_equal ; first by apply IHt', ltnW.
      by rewrite replace_at_service_at_invariant_before.
    Qed.

    Lemma replace_at_service_j_new (t' : time) (job : JobIn arr_seq) :
      (sched t != j_new) ->
      t' > t ->
      service replace_at job t' = 1 + service sched job t'.
    Proof.
      intros H_not_sched H_maybe LT.
      induction t'; first by [].
      
      (* intros t0 NEQ LT. *)
      rewrite ltnS leq_eqVlt in LT.
      move : LT => /orP [/eqP EQ | LT].
      {
        rewrite /service /service_during.
        subst t'.
        rewrite big_nat_recr //= big_nat_recr //=.
        rewrite [in RHS]addnA [1 + _]addnC -[in RHS]addnA.
        f_equal.
        {
          rewrite big_nat_cond [in RHS]big_nat_cond.
          apply eq_bigr.
          move => i /andP[// LT _].
          simpl in LT; symmetry.
          apply replace_at_service_at_invariant_other.
          by rewrite neq_ltn; apply /orP; left.
        }
        {
          rewrite /service_at /replace_at /scheduled_at.
          rewrite eq_refl H_maybe eq_refl.
          by rewrite -[_ == _]negbK H_not_sched addn0.
        }
      }
      { 
        rewrite /service /service_during.
        rewrite big_nat_recr //= big_nat_recr //=.
        rewrite [in RHS]addnA.
        f_equal; first by apply IHt'.
        symmetry.
        apply replace_at_service_at_invariant_other.
        by rewrite neq_ltn; apply /orP; right.
      }
    Qed.
    
    Lemma replace_at_service_sched_job (t' : time)(job : JobIn arr_seq):
      (sched t = Some job) ->
      (j_new <> Some job) ->
      t' > t ->
      1 + service replace_at job t' = service sched job t'.
    Proof.
      intros SCHED NEQ LT.
      induction t'; first by [].
      rewrite ltnS leq_eqVlt in LT.
      move : LT => /orP [/eqP EQ | LT].
      {
        rewrite /service /service_during.
        subst t'.
        rewrite big_nat_recr //= big_nat_recr //=.
        rewrite addnC.
        f_equal.
        {
          rewrite addnC {1}/service_at {1}/scheduled_at
                  {1}/replace_at eq_refl.
          move : NEQ => /eqP => NEQ.
          apply negbTE in NEQ. rewrite NEQ.
          rewrite add0n.                 
          apply eq_big_nat.
          intros i LT.
          simpl in LT. symmetry.
          apply replace_at_service_at_invariant_other.
          by rewrite neq_ltn; apply /orP; left.
        }
        {
          rewrite /service_at /replace_at /scheduled_at.
          by move : SCHED => /eqP->.
        }
      }
      {
        rewrite /service /service_during.
        rewrite big_nat_recr //= big_nat_recr //=.
        rewrite addnA.
        f_equal; first by apply IHt'.
        symmetry.
        apply replace_at_service_at_invariant_other.
        by rewrite neq_ltn; apply /orP; right.
      }
    Qed.

    Lemma replace_at_service_does_not_change_without_replacement
          (t' : time) (job : JobIn arr_seq):
      (sched t = Some job) ->
      (j_new = Some job) ->
      service replace_at job t' = service sched job t'.
    Proof.
      intros SCHED EQ.
      unfold service, service_during, service_at, scheduled_at.
      rewrite -EQ in SCHED.
      apply eq_big_nat.
      intros i LT. simpl in LT.
      unfold replace_at.
      rewrite -SCHED.
      desf; by [rewrite Heq | ].
    Qed.*)
    
  End Replace_At.

  Section Replace_Service.
    (* Upper level section*)
    Context {Job: eqType}.

    (* Consider any uniprocessor schedule. *)
    Context {arr_seq: arrival_sequence Job}.
    Variable sched: schedule arr_seq.
    
    Variable j_new : JobIn arr_seq.
    Variable t : time.

    Let new_sched := replace_at sched t j_new.
    Let replaced_job := sched t.

    (*
    Lemma replace_at_invariant_on_same_times:
      (replaced_job == Some j_new) -> sched t = Some j_new.
    Proof.
        by rewrite /replace_at ifN_eq.
    Qed.
    *)
    Section DifferentTimes.
      Variable t' : time.
      Lemma replace_at_invariant_on_different_times':
        t' != t -> sched t' = replace_at sched t j_new t'.
      Proof.
        by intro; rewrite /replace_at ifN_eq.
      Qed.
    End DifferentTimes.
    
    Section After.
      Variable t' : time.
      Hypothesis H_after_t : t' > t.
      
      Lemma replace_at_service_of_new_job :
        service new_sched j_new t' = service sched j_new t' + (replaced_job != Some j_new).
      Proof.
        have DIFF := replace_at_invariant_on_different_times'.
        rename H_after_t into AFTER.
        destruct t'; first by rewrite ltn0 in AFTER.
        case: (boolP (_!=_)) => EQ.
        {
          rewrite /service /service_during.
          rewrite -> big_cat_nat with (n := t);
            [simpl | by done | by apply ltnW].
          rewrite big_nat_recl //.
          rewrite {2}/service_at /scheduled_at {2}/new_sched /replace_at !eq_refl.
          rewrite [true + _]addnC addnA ; f_equal.
          rewrite -> big_cat_nat with (n := t) (p := t0.+1);
            [simpl | by done | by apply ltnW].
          rewrite big_nat_recl //.
          rewrite {4}/service_at /scheduled_at.
          apply negbTE in EQ.
          rewrite EQ add0n.
          f_equal; apply eq_big_nat.
          {
            intros i; simpl; intro LT.
            rewrite /service_at /scheduled_at.
            by rewrite DIFF; last by rewrite neq_ltn LT.
          }
          {
            move => i /andP [GE _].
            rewrite /service_at /scheduled_at.
            by rewrite DIFF // neq_ltn ltnS GE orbT.
          }
        }
        {
          rewrite /service /service_during addn0.
          apply eq_big_nat.
          intros i LT.
          rewrite /service_at /scheduled_at.
          rewrite negbK in EQ.
          move: EQ => /eqP => EQ.
          case (boolP (i == t)) => [/eqP -> | NEQi] //.
            -by rewrite /new_sched /replace_at -EQ 2!eq_refl.
            -by rewrite DIFF //.            
        }
      Qed.
      
    End After.
    
  End Replace_Service.

End Replace.