Require Import rt.util.all.
Require Import rt.model.time rt.model.job rt.model.arrival_sequence.
Require Import rt.model.uni.schedule.
From mathcomp Require Import ssreflect ssrbool eqtype ssrnat seq fintype bigop.

Module DemandBoundFunction.

  Import UniprocessorSchedule.
  
  Section ScheduleProperties.

    Context {Task: eqType}.
    Context {Job: eqType}.
    Variable job_cost: Job -> time.
    Variable job_deadline: Job -> time.
    Variable job_task: Job -> Task.
    
    (* Consider any job arrival sequence. *)
    Variable arr_seq: arrival_sequence Job.

    Section DefiningDBF.

      Let arrivals_between := jobs_arrived_between arr_seq.

      Let absolute_deadline (j: JobIn arr_seq) :=
        job_arrival j + job_deadline j.
      
     (* Let absolute_deadline *)
      
      Section PerTask.
        
        Variable tsk: Task.

        Let job_of_tsk (j: JobIn arr_seq) := job_task j == tsk.
        
        Variable t1 t2: time.

        Let jobs_with_arrival_and_deadline_between :=
        [seq j <- arrivals_between t1 t2 | absolute_deadline j < t2].

        Definition jobs_of_task_with_arrival_and_deadline_between :=
          [seq j <- jobs_with_arrival_and_deadline_between | job_of_tsk j].
        
        (* Let us define the processor demand bound of a task in the 
           interval [t1,t2) as a bound on the
           processing time requested by tsk in this interval. *)
        (*Definition demand_bound_per_task := \sum_(j <- jobs_of_task_with_arrival_and_deadline_between)    .*)
        
      End PerTask.
      
      Section EntireTaskSet.
        
        Variable t1 t2: time.
        
        (*So the processor demand of a task set is defined as the sum of all processor demands of all tasks*)
      (*Definition demand_bound ts t1 t2 := \sum_ (t_i \in taskset demand_bound_per_task) t_i t1 t2.*)

      End EntireTaskSet.

    End DefiningDBF.
    
  End ScheduleProperties.

End DemandBoundFunction.
                                                

