Require Import rt.util.all.
Require Import rt.model.time rt.model.job rt.model.arrival_sequence rt.model.priority.
Require Import rt.model.uni.schedule rt.model.uni.schedulability.
Require Import rt.model.uni.basic.platform.
Require Import rt.model.uni.transformations.swap.
Require Import rt.model.uni.transformation.construction.
From mathcomp Require Import ssreflect ssrbool eqtype ssrnat seq fintype bigop.

Module EarliestDeadlineFirst.

  Import UniprocessorSchedule Schedulability Priority Platform Swap ScheduleConstruction.

  Section Construction.
    
    Context {Job: eqType}. 
    Variable job_cost: Job -> time.
    Variable job_deadline: Job -> time.
    
    (* Consider any job arrival sequence. *)
    Variable arr_seq: arrival_sequence Job.
    (* Variable sched: schedule arr_seq. *)

    (* Let arrivals t be all jobs that have arrived by time t *)
    Let arrivals t := jobs_arrived_before arr_seq t.

    (* Filters all arrived jobs that have not completed in sched by time t. *)
    Let arrivals_not_completed (sched : schedule arr_seq) (t : time) :=
      filter (fun j => completed_by job_cost sched j t) (arrivals t).

    Section EarliestScheduled.
      
      (* We are going to define where the job j in sched is scheduled... *)
      Variable sched : schedule arr_seq.
      Variable j : JobIn arr_seq.

      (* ...in the interval [t_min, t_max). *)
      Variable t_min t_max : time.

      (* Consider the list of times that are less then t_max. *)
      Let before_maximum := map (@nat_of_ord t_max) (enum 'I_t_max).
      
      (* Next, we filter the elements that are larger then t_min. *)
      Let after_minimum := filter (fun x => x >= t_min) before_maximum.

      (* Finally, we filter the times where job j is scheduled, ... *)
      Let is_scheduled sched (j : JobIn arr_seq) := filter (fun x => scheduled_at sched j x) after_minimum.

      (* ...and take the minimum. *)
      Definition earliest_scheduled sched j := seq_min (is_scheduled sched j).
      
      Section Lemmas.
        (* Lemma: If j is not completed by t_min and completed by t_max -> earliest_scheduled != None *)
        (* Lemma: earliest_scheduled is earliest *)
      End Lemmas.
      
    End EarliestScheduled.

    Section EarliestDeadlineJob.
      
      (* Find the deadline of a job in an arrival sequence... *)
      Definition job_in_arr_seq_deadline (j : JobIn arr_seq) :=
        job_deadline (job_of_job_in j).
      
      (*  *)
      Definition earliest_deadline_job (sched : schedule arr_seq) (t_start : time) :=
        seq_argmin (job_in_arr_seq_deadline) (arrivals_not_completed sched t_start).
      
      Section Lemmas.
        
        Variable sched : schedule arr_seq.
        Variable t : time.
      
        Lemma earliest_deadline_job_exists_if_pending_not_empty :
          forall (j: JobIn arr_seq),
            j \in (arrivals_not_completed sched t) -> seq_argmin (job_in_arr_seq_deadline) (arrivals_not_completed sched t) != None.
        Proof.
          intros j H.
          by apply seq_min_exists with (x := j).
        Qed.

        Lemma earliest_deadline_job_has_earliest_deadline :
          forall x y,
            seq_argmin job_in_arr_seq_deadline (arrivals_not_completed sched t) = Some x -> y \in (arrivals_not_completed sched t) -> job_in_arr_seq_deadline x <= job_in_arr_seq_deadline y.
          intros x y HASMIN INPENDING.
          by apply seq_min_computes_min with (l := (arrivals_not_completed sched t)).
        Qed.

      End Lemmas.
      
    End EarliestDeadlineJob.
                                 
    (* ...to find a swap partner after t_start that is pending and has the least deadline. *)
    Definition find_swap_slot (sched : schedule arr_seq) (t_start : time) :=
      let swap_job := earliest_deadline_job sched t_start in
      if swap_job is Some j then
        match earliest_scheduled t_start (job_in_arr_seq_deadline j) sched j with
          | Some t => t
          | None   => t_start
        end
      else
        t_start.

    (*  *)
    (* Lemma doesnt break deadlines *)
    (* Lemma edf_swap_slot_invariant *)
    
    (* Recursively constructs an EDF schedule until time t from any valid schedule. *)
    Fixpoint edf_swap' (sched : schedule arr_seq) (t_max : time) : schedule arr_seq :=
      if t_max is t_dec.+1 then
        let rec_sched := (edf_swap' sched t_dec) in
        fun t' => (swap rec_sched (find_swap_slot rec_sched t_max) t') t'
      else
        fun t' => (swap sched (find_swap_slot sched 0) t') t'.

    Definition edf' (sched : schedule arr_seq) (t : time) :=
      (edf_swap' sched t) t.

    Variable base_sched : schedule arr_seq.
    Let edf_sched := edf' base_sched.

    (* Lemma edf_swap_slot_invariant: *)
    (*   forall t, *)
    (*     find_swap_slot (edf' base_sched) t = t. *)
    (* Proof. *)
    (*   intro t. *)
    (*   induction t. *)
    (*   rewrite /edf' /find_swap_slot. *)
    (*   case *)
    (* Qed. *)

    (* Construction using schedule prefixes *)
    Definition edf_swap (sched : schedule arr_seq) (t_max : time) : option (JobIn arr_seq) :=
      (swap sched (find_swap_slot sched t_max) t_max) t_max.
    
    Definition edf (sched : schedule arr_seq) :=
      build_schedule_from_prefixes arr_seq edf_swap sched.

    (* Maybe show a) Forall t: edf_sched t = job with earliest deadline 
                  b) edf_swap will change t to earliest deadline of pending jobs
                  c) edf_swap doesn't change all k < t*)

    (* Lemma edf_swap_slot_invariant:  *)
    (*   forall t, *)
    (*     find_swap_slot (edf base_sched) t = t. *)
    (* Proof. *)
    (*   rewrite /edf. *)
    (*   rewrite /build_schedule_from_prefixes /schedule_prefix. *)
    (*   intro t. *)
    (*   induction t using strong_ind. *)
    (* Qed. *)
    
    Lemma edf_construction_uses_swap:
        forall t,
          edf_sched t = edf_swap edf_sched t.
      Proof.
        intros t.
        induction t using strong_ind.
        (* { *)
        (*   rewrite /edf_swap /find_swap_slot. *)
        (*   set jobs := earliest_deadline_job _ _. *)
        (*   have EMPTY: jobs = None. *)
        (*   by admit. *)
        (*   rewrite EMPTY. *)
        (*   by rewrite swap_same_schedule_for_same_time. *)
        (* } *)
        {
          rewrite /edf_swap /find_swap_slot.
          rewrite /earliest_scheduled.
          admit.
        }
        (* by rewrite edf_swap_slot_invariant. *)
      Admitted.
    Print prefix_construction_uses_function.

    (* Lemma edf_depends_only_on_service : *)
    (*   forall sched1 sched2 t, *)
    (*     (forall j, service sched1 j t = service sched2 j t) -> *)
    (*     edf_swap sched1 t = edf_swap sched2 t. *)
    (* Proof. *)
    (*   intros sched1 sched2 t ALL. *)
    (*   rewrite /edf_swap /swap eq_refl. *)                                                            
                                                                  
    (* Constructs an EDF schedule for any given schedule *)
    (* Definition edf (sched : schedule arr_seq) := *)
    (*   fun t => (edf_swap sched t) t. *)
  End Construction.
  
  Section OptimalityOfEDF.

    Context {Job: eqType}. 
    Variable job_cost: Job -> time.
    Variable job_deadline: Job -> time.
    
    (* Consider any job arrival sequence. *)
    Variable arr_seq: arrival_sequence Job.
    Variable sched: schedule arr_seq.
    Variable schededf: schedule arr_seq.

    Let no_deadline_miss_in (sched: schedule arr_seq) :=
      forall j, job_misses_no_deadline job_cost job_deadline sched j.

    Hypothesis H_jobs_must_arrive_to_execute:
      jobs_must_arrive_to_execute sched.
    Hypothesis H_edf_policy: enforces_JLDP_policy job_cost schededf (EDF job_deadline).

    Let no_jobs_miss_deadlines_in_schededf := job_misses_no_deadline job_cost job_deadline schededf.

    Hypothesis H_no_deadline_misses_in_sched:
      no_deadline_miss_in sched.
    
    Lemma optimality_of_edf_schedules:
      no_deadline_miss_in schededf.
    Proof.
      rename H_no_deadline_misses_in_sched into SCHED.
      unfold no_deadline_miss_in in *.
      intros j.
    Admitted.
     
  End OptimalityOfEDF.

End EarliestDeadlineFirst.



      (* let currently_scheduled := if scheduled_at sched j t_max then *)
    (*           Some t_max *)
    (*         else *)
    (*           None in *)
    (*   if t_max is t_dec.+1 then *)
    (*     let t_rec := earliest_scheduled_after_t_min sched j t_min t_dec in *)
    (*     if t_max > t_min then *)
    (*       if t_rec is Some t_smallest then *)
    (*                Some t_smallest *)
    (*     else *)
    (*       currently_scheduled *)
    (*   else *)
    (*     currently_scheduled *)
    (* else *)
    (*   currently_scheduled. *)