Require Import rt.util.all.
Require Import rt.model.time rt.model.job rt.model.arrival_sequence rt.model.priority.
Require Import rt.model.uni.schedule rt.model.uni.schedulability.
Require Import rt.model.uni.basic.platform.
From mathcomp Require Import ssreflect ssrbool eqtype ssrnat seq fintype bigop.

Module Demand.

  Import UniprocessorSchedule Schedulability Priority Platform Job.

  (* In this section, we define the concept of demand. *)
  Section DefiningDemand.

    Context {Task: eqType}.
    Context {Job: eqType}. 
    Variable job_cost: Job -> time.
    Variable job_deadline: Job -> time.
    Variable job_task: Job -> Task.
    
    (* Consider any job arrival sequence. *)
    Variable arr_seq: arrival_sequence Job.

    (* For simplicity, let us define some local variables.*)
    Let arrivals_between := jobs_arrived_between arr_seq.
    Let absolute_deadline (j: JobIn arr_seq) :=
      job_arrival j + job_deadline j. 

    (* Flters all jobs which arrived and have to complete during [t1, t2). *)
    Definition jobs_with_arrival_and_deadline_between t1 t2 :=
      [seq j <- arrivals_between t1 t2 | absolute_deadline j < t2].

    (* In this section, we define the demand of a job set. *)
    Section TotalDemand.
        
      (* Let us define the total demand in the interval [t1,t2) as the
         workload of jobs with arrival and deadline inside the interval.*)
      Definition total_demand_during t1 t2 := 
        \sum_(j <- jobs_with_arrival_and_deadline_between t1 t2) job_cost j.
      (* Similarly the total demand before t is the demand in the interval [0, t). *)
      Definition total_demand_before t :=
        total_demand_during 0 t.
      
    End TotalDemand.

    (* In this section, we define the demand of a given task. *)
    Section DemandOfTask.

      (* Let tsk be a task ... *)
      Variable tsk: Task.

      (* ...and define a property that decides whether a job j is of tsk. *)
      Let job_of_tsk (j: JobIn arr_seq) := job_task j == tsk.

      (* Filters the jobs of tsk that arrived and have to complete in the interval [t1, t2). *)
      Definition jobs_of_task_with_arrival_and_deadline_between t1 t2 :=
        [seq j <- jobs_with_arrival_and_deadline_between t1 t2 | job_of_tsk j].

      (* Then let us define the processor demand of a task in the 
         interval [t1,t2) as the workload of jobs of tsk with 
         arrival and deadline inside this interval. *)
      Definition total_task_demand_during t1 t2 :=
        \sum_(j <- jobs_of_task_with_arrival_and_deadline_between t1 t2) job_cost j.
      Definition total_task_demand_before t :=
        total_task_demand_during 0 t.
        
    End DemandOfTask.

  End DefiningDemand.

  (* In this section, we prove some useful lemmas about demand. *)
  Section Lemmas.

    Context {Task: eqType}.
    Context {Job: eqType}. 
    Variable job_cost: Job -> time.
    Variable job_deadline: Job -> time.
    Variable job_task: Job -> Task.

    (* Consider any job arrival sequence ... *)
    Context {arr_seq: arrival_sequence Job}.

    (* ... and a schedule sched of this arrival sequence. *)
    Variable sched: schedule arr_seq.

    (* Furthermore there is a schedule which enforces EDF policy. *)
    Variable schededf: schedule arr_seq.
    Hypothesis H_edf_policy: enforces_JLDP_policy job_cost schededf (EDF job_deadline).

    (* Let all jobs have valid job parameters and only execute after they arrived. *)
    Hypothesis H_valid_job_parameters:
      forall (j: JobIn arr_seq),
        valid_realtime_job job_cost job_deadline j.
    Hypothesis H_jobs_must_arrive_to_execute:
      jobs_must_arrive_to_execute sched.

    (* For simplicity define a function which provides whether all 
       deadlines have been met in a given schedule. *)
    Let no_deadline_miss_in (sched: schedule arr_seq) :=
      forall j, job_misses_no_deadline job_cost job_deadline sched j.
    (* For now, assume that EDF is optimal which means that there 
       is an EDF schedule where all deadlines are met if there 
       is any schedule where all deadlines are met. *)
    Hypothesis H_optimality_of_edf_schedules:
      no_deadline_miss_in sched -> no_deadline_miss_in schededf. 

    (* For simplicity, let us define some local variables about demand. *)
    Let absolute_deadline (j: JobIn arr_seq) :=
      job_arrival j + job_deadline j.
    Let demand_during := total_demand_during job_cost job_deadline arr_seq.
    Let demand_before := total_demand_before job_cost job_deadline arr_seq.

    (* In this section, we prove properties if a job set is schedulable. *)
    Section Schedulability.

      (* Assume that the job set is schedulable. *)
      Hypothesis H_no_deadline_miss:
        no_deadline_miss_in sched.

      (* Has to go to a different file. *)
      (* The total service providable is bounded by the length of the interval. *)
      Lemma total_service_bounded_by_interval_length:
        forall t delta, total_service_during sched t (t + delta) <= delta.
      Proof.
        Admitted.

      (* Then the total service is not greater than the total demand. *)
      Lemma total_service_ge_total_demand_if_schedulable:
        forall t delta, demand_during t (t + delta) <= total_service_during sched t (t + delta).
      Proof.
        Admitted.
      
      (* As a corollary, the demand will never exceed the length of the interval. *)
      Lemma demand_never_exceeds_time_if_schedulable:
         forall t delta, demand_during t (t + delta) <= delta.
       (*service received by j at deadline will be not enough.*)
      Proof.
        rename H_optimality_of_edf_schedules into OPTEDF,
               H_no_deadline_miss into NOMISS.
        apply OPTEDF in NOMISS.
                induction t.
        {
          admit.
        }
        intros delta. apply contraT.
        rewrite -ltnNge.
        (*have SERVLTT : forall j, service_during sched j 0 t0.+1 <= t0.+1.*)
        {
          admit.
        }
        Admitted.
                
    End Schedulability.  

    (* In this section, we introduce properties which you have to  
       check in order to check schedulability. *)
    Section SatisfiedDemand.

      (*If the demand during any interval is bounded by the length of the interval, ... *)
      Hypothesis H_demand_always_satisfied:
        forall t delta, demand_during t (t + delta) <= delta.

      (*...then the job set is schedulable. *)
      Lemma schedulable_if_demand_always_satisfied:
        no_deadline_miss_in sched.
      Proof.
        rename H_valid_job_parameters into VALID.
        rename H_demand_always_satisfied into SAT.
        rewrite /no_deadline_miss_in.
        suff NOMISS : forall t j, job_arrival j + job_deadline j <= t ->
                          job_misses_no_deadline job_cost job_deadline sched j.
        {
          intro j.
          by apply (NOMISS (job_arrival j + job_deadline j)).
        }
        induction t.
        {
          intros j.
          rewrite leqn0 addn_eq0.
          have DEAD0 : (job_deadline j == 0) = false.
          {
            apply negbTE. rewrite -lt0n.
            rewrite /valid_realtime_job in VALID.
            apply VALID.
          }
          by rewrite DEAD0 // andbF.
        }
        intros j LE.
        (*j is the first job to miss its deadline.*)
        case: (leqP (job_arrival j + job_deadline j) (t)) => [LEQ | GT].
        -by apply IHt.
        have TDEAD : (job_arrival j + job_deadline j) == t.+1.
        {
          rewrite eqn_leq.
          rewrite LE //.
        }
        clear LE GT IHt.
        apply contraT.  
        (* For now, we know that j is the first job to miss its deadline
           and the deadline is at t.+1. *)
        (*proof sketch:
          exists t1 so that [t1, t.+1) is a busy interval.
           -> t1 is quiet time, every point in interval is busy
          there is a deadline miss at t.+1 
           => demand t1 t.+1 > (t.+1 - t1) 
          contradiction.
        *)  
        Admitted.
        
    End SatisfiedDemand.  

    (* In this section, we combine the lemmas from the two sections 
       before so that we know equivalence of both properties. *)
    Section Iff.

      Variable t : time.
       
      Lemma schedulable_iff_demand_always_satisfied:
        no_deadline_miss_in sched <->
        forall t delta, demand_during t (t +delta) <= delta.
      Proof.
        rewrite iff_to_and.
        split.
        {
          intro NOMISS.
          by apply demand_never_exceeds_time_if_schedulable.
        }
        {
          intro SCHED.
          by apply schedulable_if_demand_always_satisfied.
        }
      Qed.   
         
    End Iff.
    
  End Lemmas.

End Demand.